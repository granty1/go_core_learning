package main

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"github.com/granty1/gin-auth/models"
	"github.com/granty1/gin-auth/pkg/setting"
	"github.com/granty1/gin-auth/routers"
	"net/http"
)

func main() {
	r := routers.InitRouter()

	r.GET("/test", func(c *gin.Context) {
		c.JSON(http.StatusOK, gin.H{
			"data": models.GetAllAuths(),
		})
	})

	server := &http.Server{
		Addr:           fmt.Sprintf(":%d", setting.Port),
		Handler:        r,
		ReadTimeout:    setting.ReadTimeout,
		WriteTimeout:   setting.WriteTimeout,
		MaxHeaderBytes: 1 << 20,
	}

	server.ListenAndServe()
}
