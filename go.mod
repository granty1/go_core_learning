module github.com/granty1/gin-auth

go 1.13

replace (
	github.com/granty1/gin-auth/conf => ./conf
	github.com/granty1/gin-auth/middleware => ./middleware
	github.com/granty1/gin-auth/models => ./models
	github.com/granty1/gin-auth/pkg => ./pkg
	github.com/granty1/gin-auth/pkg/e => ./pkg/e
	github.com/granty1/gin-auth/pkg/setting => ./pkg/util
	github.com/granty1/gin-auth/routers => ./routers
)

require (
	github.com/astaxie/beego v1.12.0
	github.com/gin-contrib/sse v0.1.0 // indirect
	github.com/gin-gonic/gin v1.4.0
	github.com/go-ini/ini v1.48.0
	github.com/go-sql-driver/mysql v1.4.1
	github.com/golang/protobuf v1.3.2 // indirect
	github.com/jinzhu/gorm v1.9.11
	github.com/json-iterator/go v1.1.7 // indirect
	github.com/kr/pretty v0.1.0 // indirect
	github.com/mattn/go-isatty v0.0.10 // indirect
	github.com/smartystreets/goconvey v0.0.0-20190731233626-505e41936337 // indirect
	github.com/ugorji/go v1.1.7 // indirect
	github.com/unknwon/com v1.0.1
	golang.org/x/sys v0.0.0-20191010194322-b09406accb47 // indirect
	google.golang.org/appengine v1.6.5 // indirect
	gopkg.in/ini.v1 v1.48.0 // indirect
	gopkg.in/yaml.v2 v2.2.4 // indirect
)
