package v1

import (
	"github.com/astaxie/beego/validation"
	"github.com/gin-gonic/gin"
	"github.com/granty1/gin-auth/models"
	"github.com/granty1/gin-auth/pkg/e"
	"github.com/granty1/gin-auth/pkg/setting"
	"github.com/granty1/gin-auth/pkg/util"
	"github.com/unknwon/com"
	"net/http"
)

/**
获取多个文章标签
 */
func GetTags(c *gin.Context) {
	name := c.Query("name")

	maps := make(map[string]interface{})
	data := make(map[string]interface{})

	if name != "" {
		maps["name"] = name
	}
	var state int = -1
	if arg := c.Query("state"); arg != "" {
		state = com.StrTo(arg).MustInt()
		maps["state"] = state
	}

	code := e.SUCCESS

	data["list"] = models.GetTags(util.GetPage(c), setting.PageSize, maps)
	data["total"] = models.GetTagsCount(maps)

	c.JSON(http.StatusOK, gin.H{
		"code": code,
		"msg": e.GetMsg(code),
		"data": data,
	})
}

/**
添加标签
 */
func AddTag(c *gin.Context) {
	name := c.Query("name")
	state := com.StrTo(c.DefaultQuery("state", "0")).MustInt()
	createdBy := c.Query("created_by")

	validate := validation.Validation{}
	validate.Required(name, "name").Message("Name can't be null.")
	validate.MaxSize(name, 100, "name").Message("Name maxnum is 100.")
	validate.Required(createdBy, "created_by").Message("Created by can't be null.")
	validate.MaxSize(createdBy, 100, "created_by").Message("Created by Maxnum is 100.")
	validate.Range(state, 0, 1, "state").Message("State just be 1/0.")

	code := e.INVALID_PARAMS
	if !validate.HasErrors() {
		if !models.ExistTagByName(name) {
			code = e.SUCCESS
			models.AddTag(name, state, createdBy)
		} else {
			code = e.ERROR_EXIST_TAG
		}
	}

	c.JSON(http.StatusOK, gin.H{
		"code": code,
		"msg": e.GetMsg(code),
		"data": nil,
	})
}

/**
修改标签
 */
func EditTag(c *gin.Context) {
	id := com.StrTo(c.Param("id")).MustInt()
	name := c.Query("name")
	modifiedBy := c.Query("modified_by")

	valid := validation.Validation{}

	var state int = -1
	if arg := c.Query("state"); arg != "" {
		state = com.StrTo(arg).MustInt()
		valid.Range(state, 0, 1, "state").Message("State is just be 1/0.")
	}

	valid.Required(id, "id").Message("ID can't be null.")
	valid.Required(modifiedBy, "modified_by").Message("Modify by can't be null.")
	valid.MaxSize(modifiedBy, 100, "modified_by").Message("Modify by maxnum is 100.")
	valid.MaxSize(name, 100, "name").Message("Name maxnum is 100")

	code := e.INVALID_PARAMS
	if ! valid.HasErrors() {
		code = e.SUCCESS
		if models.ExistTagByID(id) {
			data := make(map[string]interface{})
			data["modified_by"] = modifiedBy
			if name != "" {
				data["name"] = name
			}
			if state != -1 {
				data["state"] = state
			}

			models.EditTag(id, data)
		} else {
			code = e.ERROR_NOT_EXIST_TAG
		}
	}

	c.JSON(http.StatusOK, gin.H{
		"code" : code,
		"msg" : valid.Errors,
		"data" : make(map[string]string),
	})
}

/**
删除标签
 */
func DeleteTag(c *gin.Context) {
	id := com.StrTo(c.Param("id")).MustInt()

	validate := validation.Validation{}
	validate.Min(id, 1, "id").Message("Id must > 0")

	code := e.INVALID_PARAMS
	if !validate.HasErrors() {
		if models.ExistTagByID(id) {
			models.DeleteTag(id)
			code = e.SUCCESS
		} else {
			code = e.ERROR_NOT_EXIST_TAG
		}
	}

	c.JSON(http.StatusOK,
		gin.H{
			"code": code,
			"msg": validate.Errors,
			"data": nil,
		})
}
