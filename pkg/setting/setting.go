package setting

import (
	"github.com/go-ini/ini"
	"log"
	"os"
	"time"
)

var (
	Cfg *ini.File

	RunMode string

	Port int

	ReadTimeout time.Duration
	WriteTimeout time.Duration

	PageSize int
	JwtSecret string

	DBType, DBName, DBuser, DBpassword, DBhost, DBTablePrefix string
)


func init() {
	var err error
	Cfg, err = ini.Load("conf/app.ini")

	if err != nil {
		log.Fatalf("[Init] Fail to load app.ini ,error:%v\n", err)
		os.Exit(0)
	}

	loadBase()
	loadServer()
	loadDataBase()
	loadApp()
}

func loadBase() {
	RunMode = Cfg.Section("").Key("RUN_MODE").MustString("debug")
}

func loadServer() {
	server, err := Cfg.GetSection("server")
	if err != nil {
		log.Fatalf("[Init] Fail to read server section, error:%v\n", err)
	}

	Port = server.Key("HTTP_PORT").MustInt(8000)
	ReadTimeout = server.Key("READ_TIMEOUT").MustDuration( 60 * time.Second)
	WriteTimeout = server.Key("WRITE_TIMEOUT").MustDuration( 60 * time.Second)
}

func loadDataBase() {
	db, err := Cfg.GetSection("database")
	if err != nil {
		log.Fatalf("[Init] Fail to read database section, error:%v\n", err)
		os.Exit(0)
	}

	DBType = db.Key("TYPE").MustString("mysql")
	DBhost = db.Key("HOST").MustString("127.0.0.1:3306")
	DBName = db.Key("NAME").MustString("database_name")
	DBTablePrefix = db.Key("TABLE_PREFIX").MustString("prefix_")
	DBuser = db.Key("USER").MustString("root")
	DBpassword = db.Key("PASSWORD").MustString("#")

}

func loadApp() {
	app, err := Cfg.GetSection("app")
	if err != nil {
		log.Fatalf("[Init] Fail to read app section, error:%v\n", err)
	}

	PageSize = app.Key("PAGE_SIZE").MustInt(10)
	JwtSecret = app.Key("JWT_SECRET").MustString("!@#$%")
}
