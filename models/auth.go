package models

type Auth struct {
	ID       int    `json:"id"`
	Username string `json:"username"`
	Password string `json:"password"`
}


func GetAllAuths() (auths []Auth) {
	db.Table("blog_auth").Find(&auths)
	return
}
