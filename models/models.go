package models

import (
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"github.com/granty1/gin-auth/pkg/setting"
	"github.com/jinzhu/gorm"
	"log"
)

type Model struct {
	ID         int `gorm:"primary_key" json:"id"`
	CreatedOn  int `json:"created_on"`
	ModifiedOn int `json:"modified_on"`
}



var db *gorm.DB
var channel = make(chan int)

func init() {
	var err error
	db, err = gorm.Open(setting.DBType, fmt.Sprintf("%s:%s@(%s)/%s?charset=utf8&parseTime=True&loc=Local",
		setting.DBuser,
		setting.DBpassword,
		setting.DBhost,
		setting.DBName))
	if err != nil {
		log.Fatalf("Models Init open mysql fail.%+v\n", err)
	}

	gorm.DefaultTableNameHandler = setDefaultTableName

	db.SingularTable(true)
	db.LogMode(true)

	db.DB().SetMaxIdleConns(10)
	db.DB().SetMaxOpenConns(100)


}

func setDefaultTableName(db *gorm.DB, defaultTableName string) string {
	return setting.DBTablePrefix + defaultTableName
}

func closeDB() {
	defer db.Close()
}

